const RESULT = {
  resolve: 'Promise fulfilled',
  reject: 'reject rejected',
};

const callPromise = () => new Promise((resolve, reject) => {
  const success = (Math.floor(Math.random() * 200) + 1 ) > 100;

  setTimeout(() => {
    if (success) {
      resolve(RESULT.resolve);
    } else {
      reject(new Error(RESULT.reject));
    }
  }, 900);
});

async function task1() {
  callPromise()
    .then(res => console.log(res))
    .catch(() => alert('Произошла ошибка'))
}

async function task2() {
  try {
    const res = await callPromise();
    console.log(res);
  }
  catch(e) {
    alert('Произошла ошибка');
  }

}

task1();

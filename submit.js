const form = document.querySelector('#myForm');
const hiddenInput = (document.querySelector('input[type="hidden"]')).parentNode;

const newInput = document.createElement('input');
newInput.name = 'newField';
newInput.setAttribute('value', 'Форма отправлена');
newInput.classList.add('field');
newInput.style.border = 'none'

form.addEventListener('submit', (e) => {
    e.preventDefault();
    form.appendChild(newInput);
    form.removeChild(hiddenInput);
    form.submit();
})